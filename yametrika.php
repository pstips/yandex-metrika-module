<?php

if (!defined('_PS_VERSION_'))
	exit;

class yametrika extends Module{

	private $_html = '';
	private $_postErrors = array();

	public function __construct()
   {
      $this->name = 'yametrika';
      $this->version = '1.0';
		$this->author = 'max4d';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();	
		$this->displayName = $this->l('Модуль Яндекс-Метрика');
		$this->description = $this->l('');
	}
	
	public function install()
	{
		if (!parent::install() || !$this->registerHook('displayFooter'))
			return false;
		return true;
	}
		
	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}

   public function getContent()
   {
     $var_array = array('YANDEX_METRIKA_CODE', 'YANDEX_METRIKA_CODE_ORDERCONFIRM');
     foreach  ($var_array as $var)
     {
       if (array_key_exists($var, $_POST)) 
       {
         Configuration::updateValue($var, $_POST[$var], true);
       }      
       $this->context->smarty->assign($var, Configuration::get($var));    
     }         
     return $this->display(__FILE__, 'adminform.tpl');
   }

	public function hookdisplayFooter()
	{			  
	  if($this->context->controller->php_self != "order-confirmation") 
	  {
       return "&nbsp;&nbsp;&nbsp;" . Configuration::get('YANDEX_METRIKA_CODE');
     }
     else 
     {
     	 $CODE_ORDERCONFIRM = str_replace("{/*Здесь параметры визита*/}", $this->get_json_order_param(), Configuration::get('YANDEX_METRIKA_CODE_ORDERCONFIRM'));
       return "&nbsp;&nbsp;&nbsp;" . $CODE_ORDERCONFIRM;
     }
	}
	
	private function get_json_order_param()
	{
	  $order = new Order($this->context->controller->id_order);
	  
	  $products = $order->getProducts();	  
	  $order_items = array();
	  
	  foreach ($products as $id_product => $product_arr)
	  {
	    $order_items[] = array("id" =>$product_arr["product_id"], "name"=>$product_arr["product_name"], "price"=>$product_arr["product_price"], "quantity"=>$product_arr["product_quantity"]);
	  }
	  
	  $order_params = array("order_id"=>$this->context->controller->id_order,"order_price"=>$order->total_paid_real,"currency"=>$order->id_currency, "goods"=>$order_items);
	  
	  return json_encode($order_params);	
	}
}

	



